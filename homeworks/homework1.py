import sys
from matplotlib import pyplot as plt
import numpy as np

sys.path.append("/home/pulkit/data/pulkit/Documents/PDF/umass/cs687/HW1/rl-framework-687-public")

from rl687.environments.gridworld import Gridworld

_seed = 482
np.random.seed(_seed)

def _getActionA():
    # Choose a random action
    act = np.random.randint(4)

    # Model Broken agent Behavior
    rand_act = np.random.rand()
    if (rand_act < 0.80):
        act = act
    elif (rand_act < 0.85):
        switcher = {
            0: 2,
            1: 3,
            2: 1,
            3: 0,
        }
        act = switcher.get(act, 4)
    elif (rand_act < 0.90):
        switcher = {
            0: 3,
            1: 2,
            2: 0,
            3: 1,
        }
        act = switcher.get(act, 4)
    elif (rand_act < 1):
        act = 4
    return act

def problemA(gw):
    """
    Have the agent uniformly randomly select actions. Run 10,000 episodes.
    Report the mean, standard deviation, maximum, and minimum of the observed 
    discounted returns.
    """
    disc_returns = []
    for episode in range(1, 10001):
        if (episode % 100 == 0):
            print("Episode :", episode, end='\r')
        # print("Resetting ...")
        gw.reset()
        disc_return = 0
        time_step = 0
        while (gw.isEnd == False):
            act = _getActionA()
            gw.step(act)    
            Rt = gw.R(gw._state)
            disc_return += (gw._gamma**time_step)*Rt
            time_step += 1
        disc_returns.append(disc_return)
    print()
    return disc_returns


opt = np.array([[3, 3, 3, 3, 1], 
                [0, 3, 3, 3, 1],
                [0, 1, 3, 3, 1],
                [0, 2, 3, 3, 1],
                [0, 2, 3, 3, 1]])

def _getActionB(state):
    # Choose from optimal policy array
    act = opt[state[0], state[1]]

    # Model Broken agent Behavior
    rand_act = np.random.rand()
    if (rand_act < 0.80):
        act = act
    elif (rand_act < 0.85):
        switcher = {
            0: 2,
            1: 3,
            2: 1,
            3: 0,
        }
        act = switcher.get(act, 4)
    # if (rand_act == 18):
    elif (rand_act < 0.90):
        switcher = {
            0: 3,
            1: 2,
            2: 0,
            3: 1,
        }
        act = switcher.get(act, 4)
    # if (rand_act == 19 or rand_act == 20):
    elif (rand_act < 1):
        act = 4
    return act


def problemB(gw):
    """
    Run the optimal policy that you found for 10,000 episodes. Report the 
    mean, standard deviation, maximum, and minimum of the observed 
    discounted returns
    """
    disc_returns = []
    act_seqs = []
    max_disc_ret = 0
    max_id = 0
    for episode in range(1, 10001):
        if ((episode + 1) % 100 == 0):
            print("Episode :", (episode + 1), end='\r')
        # print("Resetting ...")
        gw.reset()
        disc_return = 0
        act_seq = []
        time_step = 0
        while (gw.isEnd == False):
            act = _getActionB(gw._state)
            gw.step(act)
            Rt = gw.R(gw._state)
            disc_return += (gw._gamma**time_step)*Rt
            time_step += 1
            act_seq.append(act)
        act_seqs.append(act_seq)
        disc_returns.append(disc_return)
        if (disc_return > max_disc_ret):
            max_id = episode
            max_disc_ret = disc_return
    print()
    # print(max_id, max_disc_ret)
    # Print the states from best action sequence
    # print(len(act_seqs[max_id]))
    # gw.reset()
    # for act in act_seqs[max_id]:
    #     gw.step(act)
    #     print(gw._state)

    return disc_returns
    
def getcdf(arr):
    # Create some test data
    dx = (5 - (-5)) / len(arr)
    X  = np.arange(-5, 5, dx)
    Y  = arr

    # Normalize the data to a proper PDF
    x = -5
    Ys = []
    while (x < 5):
        Ym = np.where(Y <= x)
        Yt = Y[Ym]
        # print(Yt)
        x += dx
        Ys.append(Yt.sum()/len(Y))
    # plot(X, Ys, 'r--')

def getQuantile(disc_rets):    
    disc_rets_sorted = np.sort(disc_rets)
    dx = (1 - 0) / len(disc_rets_sorted)
    quantile_domain = np.arange(0, 1, dx)

    line, = plt.plot(quantile_domain, disc_rets_sorted)
    plt.xlabel("Tau")
    plt.ylabel("x")

def getQuantiles(disc_rets_arr):
    quantile_domains = []
    quantile_ranges = []
    for disc_rets in disc_rets_arr:
        disc_rets_sorted = np.sort(disc_rets)
        dx = (1 - 0) / len(disc_rets_sorted)
        quantile_domain = np.arange(0, 1, dx)
        quantile_domains.append(quantile_domain)
        quantile_ranges.append(disc_rets_sorted)

    line1, = plt.plot(quantile_domains[0], quantile_ranges[0])
    line1.set_label("Uniformly Random policy")
    line2, = plt.plot(quantile_domains[1], quantile_ranges[1])
    line2.set_label("Optimal policy")
    plt.gca().set_ylim([-60, 10])
    plt.legend()
    plt.xlabel("T")
    plt.ylabel("Reward")
    # show()

def problemApartE(gw):
    """
    Have the agent uniformly randomly select actions. Run 10,000 episodes.
    Report the mean, standard deviation, maximum, and minimum of the observed 
    discounted returns.
    """
    success = 0
    for episode in range(1, 10001):
        if (episode % 100 == 0):
            print("Episode :", episode, end='\r')
        # print("Resetting ...")
        gw.reset()
        disc_return = 0
        time_step = 0
        while (gw.isEnd == False):
            act = _getActionA()
            gw.step(act)
            if (gw._state == (4, 2) and time_step == 11):
                success += 1
            Rt = gw.R(gw._state)
            time_step += 1

    print()
    return success/10000


def solvePartB():
    # Solve problemA, part B
    np.random.seed(_seed)
    gw = Gridworld()
    disc_retsA = problemA(gw)
    disc_retsA = np.array(disc_retsA)
    meanA = np.mean(disc_retsA)
    stdA = np.std(disc_retsA)
    maxiA = np.amax(disc_retsA)
    miniA = np.min(disc_retsA)
    print(meanA, stdA, maxiA, miniA)

def solvePartC():
    np.random.seed(_seed)
    gw = Gridworld()
    disc_retsB = problemB(gw)
    disc_retsB = np.array(disc_retsB)
    meanB = np.mean(disc_retsB)
    stdB = np.std(disc_retsB)
    maxiB = np.amax(disc_retsB)
    miniB = np.min(disc_retsB)
    print(meanB, stdB, maxiB, miniB)

def solvePartD():
    np.random.seed(_seed)
    gw = Gridworld()
    disc_retsA = problemA(gw)
    disc_retsA = np.array(disc_retsA)
    meanA = np.mean(disc_retsA)
    stdA = np.std(disc_retsA)
    maxiA = np.amax(disc_retsA)
    miniA = np.min(disc_retsA)
    print(meanA, stdA, maxiA, miniA)

    np.random.seed(_seed)
    gw = Gridworld()
    disc_retsB = problemB(gw)
    disc_retsB = np.array(disc_retsB)
    meanB = np.mean(disc_retsB)
    stdB = np.std(disc_retsB)
    maxiB = np.amax(disc_retsB)
    miniB = np.min(disc_retsB)
    print(meanB, stdB, maxiB, miniB)

    getQuantiles([disc_retsA, disc_retsB])
    plt.show()

def solvePartE():
    gw = Gridworld(19, 24, (5,5), [12, 17], [6, 18, 22])
    prob = problemApartE(gw)
    print(prob)

def main():  
    solvePartB()
    # solvePartC()
    # solvePartD()
    # solvePartE()

if __name__ == "__main__":
    main()