import numpy as np
from .skeleton import Environment

class Gridworld(Environment):
    """
    The Gridworld as described in the lecture notes of the 687 course material. 
    
    Actions: up (0), down (1), left (2), right (3)
    
    Environment Dynamics: With probability 0.8 the robot moves in the specified
        direction. With probability 0.05 it gets confused and veers to the
        right -- it moves +90 degrees from where it attempted to move, e.g., 
        with probability 0.05, moving up will result in the robot moving right.
        With probability 0.05 it gets confused and veers to the left -- moves
        -90 degrees from where it attempted to move, e.g., with probability 
        0.05, moving right will result in the robot moving down. With 
        probability 0.1 the robot temporarily breaks and does not move at all. 
        If the movement defined by these dynamics would cause the agent to 
        exit the grid (e.g., move up when next to the top wall), then the
        agent does not move. The robot starts in the top left corner, and the 
        process ends in the bottom right corner.
        
    Rewards: -10 for entering the state with water
            +10 for entering the goal state
            0 everywhere else
        
    
    
    """

    ## Obstacles = -1
    ## Water states = -2
    ## start state = 1
    ## endstate = 2
    def __init__(self, startState=0, endState=24, shape=(5,5), obstacles=[12, 17], waterStates=[6, 18, 22]):
        self._name = "MoreWateryGridWorld"
        self._startState = startState
        self._endState = endState
        self._shape = shape
        self._grid = np.zeros(shape, dtype=np.int32)
        self._initialize_grid(startState, endState, shape, obstacles, waterStates)
        self._state = (0, 0)
        self._gamma = 0.9
        self._isEnd = False
        self._reward = 0
        self._rewardFn = np.zeros(shape)
        self._initialize_rewardFn(startState, endState, shape, obstacles, waterStates)
        self._timestep = 0
        return

    def _initialize_grid(self, startState, endState, shape, obstacles, waterStates): 
        self._grid[self._startState // self._shape[1], self._startState % self._shape[1]] = 1
        self._grid[self._endState // self._shape[1], self._endState % self._shape[1]] = 2
        for obs in obstacles:
            self._grid[obs // self._shape[1], obs % shape[1]] = -1
        for wSt in waterStates:
            self._grid[wSt // self._shape[1], wSt % shape[1]] = -2
        return 

    def _initialize_rewardFn(self, startState, endState, shape, obstacles, waterStates):
        self._rewardFn[self._endState // shape[1], self._endState % shape[1]] = +10
        for wst in waterStates:
            self._rewardFn[wst // self._shape[1], wst % self._shape[1]] = -10
        return

    @property
    def name(self):
        return "MoreWateryGridWorld"
        
    @property
    def reward(self):
        return self._reward

    @property
    def action(self):
        return self._action

    @property
    def isEnd(self):
        return self._isEnd

    @property
    def state(self):
        return self._state

    @property
    def gamma(self):
        return self._gamma

    ## Simulate actions
    def _sim_up(self):
        if (self._state[0] > 0):
            self._state = (self._state[0] - 1, self._state[1])
        if (self._grid[self._state] == -1):
            self._state = (self._state[0] + 1, self._state[1])
        return
    
    def _sim_down(self):
        if (self._state[0] < self._shape[0] - 1):
            self._state = (self._state[0] + 1, self._state[1])
        if (self._grid[self._state] == -1):
            self._state = (self._state[0] - 1, self._state[1])
        return
    def _sim_left(self):
        if (self._state[1] > 0):
            self._state = (self._state[0], self._state[1] - 1)
        if (self._grid[self._state] == -1):
            self._state = (self._state[0], self._state[1] + 1)
        return

    def _sim_right(self):
        if (self._state[1] < self._shape[-1] - 1):
            self._state = (self._state[0], self._state[1] + 1)
        if (self._grid[self._state] == -1):
            self._state = (self._state[0], self._state[1] - 1)
        return

    def _sim_stay(self):
        return

    def step(self, action):
        # For now let us assume "action" is deterministic
        actionfn = {
            0: self._sim_up,
            1: self._sim_down,
            2: self._sim_left,
            3: self._sim_right,
            4: self._sim_stay,
        }
        actionFnHandle = actionfn.get(action, lambda: "Invalid action")
        # Update state
        actionFnHandle()

        self._reward = self._rewardFn[self._state]
        if (self._grid[self._state] == 2):
            self._isEnd = True
        self._timestep += 1
        return
        #return _state, _reward, _isEnd

    def reset(self):
        self._state = (0, 0)
        self._reward = 0
        self._isEnd = False
        return
        
    def R(self, _state):
        """
        reward function
        
        output:
            reward -- the reward resulting in the agent being in a particular state
        """
        # discount = self._gamma**self._timestep
        return self._rewardFn[_state]

    def print_grid(self):
        temp = self._grid[self._state[0], self._state[1]]
        self._grid[self._state[0], self._state[1]] = -5
        print(self._grid)
        self._grid[self._state[0], self._state[1]] = temp
        # print ("----------------------------------")
        # for row in self._grid :
        #     print ("| ", end='')
        #     for col in row:
        #         if (col == -5):
        #             print("H | ", end = '')
        #         else:
        #             print(str(col) + " | ", end='')
        #     print()
        #     print ("----------------------------------")
        return
