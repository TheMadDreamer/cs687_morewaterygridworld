from environments.gridworld import *
from environments.skeleton import *

if __name__=="__main__":
    gw = Gridworld()
    print("Printing Gridworld ...")
    gw.print_grid()
    gw.step(3)
    gw.step(3)
    gw.step(3)
    gw.step(3)
    gw.step(1)
    gw.step(1)
    gw.step(1)
    gw.step(1)
    gw.step(1)
    print(gw.R(gw._state))
    gw.print_grid()
